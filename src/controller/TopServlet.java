package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserComment;
//import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
//import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String seachcategory = request.getParameter("seachcategory");
		//ユーザーのログインを判断し、投稿の表示の判断を行う
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		request.setAttribute("start", start);
		request.setAttribute("end", end);
		request.setAttribute("seachcategory", seachcategory);

		if (StringUtils.isEmpty(start)==true && StringUtils.isEmpty(end) == true){
			start = ("0000-10-01-00:00:00");
			end = ("9999-12-31-00:00:00");
		}else if( StringUtils.isEmpty(start) == true ){
			start = ("0000-10-01-00:00:00");
		}else if( StringUtils.isEmpty(end) == true ){
			end = ("9999-12-31-00:00:00");
		}

		List<UserMessage> messages = new MessageService().getMessage(start, end, seachcategory);
		List<UserComment> comments = new CommentService().getComment();



		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

}
