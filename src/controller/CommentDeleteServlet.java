package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exception.NoRowsUpdatedRuntimeException;
import service.CommentService;

@WebServlet(urlPatterns = { "/commentDelete" })
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> deleteComments = new ArrayList<String>();

		int id = Integer.parseInt(request.getParameter("comment_id"));

		try {
			new CommentService().delete(id);
		} catch (NoRowsUpdatedRuntimeException e) {
			deleteComments.add("失敗しました。");
			response.sendRedirect("./");
		}


		response.sendRedirect("./");
	}
}