package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.BranchList;
import beans.PositionList;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchListService;
import service.PositionListService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		String selectUser = request.getParameter("id");

		// 直接入力による編集画面に移行する際のエラー処理
		if (StringUtils.isEmpty(selectUser)) {
			messages.add("パラメータが不正です");
			session.setAttribute("errorMessages", messages);
			System.out.println("IDがない場合のエラー");
			response.sendRedirect("userlist");
			return;
		}

		if (selectUser.matches("\\d*") == false) {
			messages.add("パラメータが不正です");
			session.setAttribute("errorMessages", messages);
			System.out.println("IDが数字以外の場合のエラー");
			response.sendRedirect("userlist");
			return;
		}



		User editUser = new UserService().getUser(Integer.parseInt(selectUser));
		request.setAttribute("editUser", editUser);

		if (editUser == null) {
			messages.add("パラメータが不正です");
			session.setAttribute("errorMessages", messages);
			System.out.println("IDパラメータがない場合のエラー");
			response.sendRedirect("userlist");
			return;
		}

		List<BranchList> branchList = new BranchListService().getBranchList();
		request.setAttribute("branchLists", branchList);

		List<PositionList> positionList = new PositionListService().getPositionList();
		request.setAttribute("positionLists", positionList);

		request.setAttribute("errorMessages", messages);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		User editUser = getEditUser(request);
		request.setAttribute("editUser", editUser);

		List<BranchList> branchList = new BranchListService().getBranchList();
		request.setAttribute("branchLists", branchList);

		List<PositionList> positionList = new PositionListService().getPositionList();
		request.setAttribute("positionLists", positionList);

		if (isValid(request, messages, editUser) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				// messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				request.setAttribute("errorMessages", messages);
				response.sendRedirect("settings");
			}

			request.setAttribute("id", editUser);

			response.sendRedirect("userlist");
		} else {
			request.setAttribute("editUser", editUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
			// response.sendRedirect("settings");
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchLists")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionLists")));

		return editUser;

	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User editUser) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String userName = request.getParameter("name");
		int selectUser = Integer.parseInt(request.getParameter("id"));
		int branchId = Integer.parseInt(request.getParameter("branchLists"));
		int positionId = Integer.parseInt(request.getParameter("positionLists"));

		User check = new UserService().checkId(login_id);
//		int checkId = check.getId();


		// 編集の場合、編集対象のID重複を避ける
		if (StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}

		if (check != null && check.getId() != selectUser) {
				messages.add("ログインIDがすでに使用されています");
		}


		if (login_id.matches("\\W*")) {
			messages.add("ログインIDは半角英数で入力してください");
		}

		if (20 < login_id.length() || login_id.length() < 6) {
			messages.add("ログインIDは6文字以上、20文字以下で入力してください");
		}

		if (20 < password.length()) {
			messages.add("パスワードは6文字以上、20文字以下で入力してください");
		}

		if (!(StringUtils.isEmpty(password)) && (password.length() < 6)) {
			messages.add("パスワードは6文字以上、20文字以下で入力してください");
		}


		if (password.equals(passwordConfirm) == false) {
			messages.add("確認パスワードが一致しておりません");
		}

		if (StringUtils.isBlank(userName) == true) {
			messages.add("名前を入力してください");
		}

		if (10 < userName.length()) {
			messages.add("名前は10文字以下で入力してください");
		}

		// if((selectUser == null) == true){
		// messages.add("URLが不正です");
		// }

		if ((branchId == 1) && (3 <= positionId)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		} else if ((branchId != 1) && (1 <= positionId && positionId <= 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}

		// 支店コードは実際の処理がint型のため、こちらでエラー処理（String型）で行うとした場合、
		// エラーが起きている。

		// if(editUser.getBranchId().matches("\\D*") == true) {
		// messages.add("支店コードは半角数値で入力してください");
		// }
		// TODO アカウントが既に利用されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}