package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exception.NoRowsUpdatedRuntimeException;
import service.MessageService;

@WebServlet(urlPatterns = { "/messageDelete" })
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> deleteMessages = new ArrayList<String>();

		int id = Integer.parseInt(request.getParameter("id"));

		try {
			new MessageService().delete(id);
		} catch (NoRowsUpdatedRuntimeException e) {
			deleteMessages.add("失敗しました。");
			response.sendRedirect("./");
		}


		response.sendRedirect("./");
	}
}