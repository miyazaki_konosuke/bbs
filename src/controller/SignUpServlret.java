package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.BranchList;
import beans.PositionList;
import beans.User;
import service.BranchListService;
import service.PositionListService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlret extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<BranchList> branchList = new BranchListService().getBranchList();
		request.setAttribute("branchLists", branchList);

		List<PositionList> positionList = new PositionListService().getPositionList();
		request.setAttribute("positionLists", positionList);



		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		List<BranchList> branchList = new BranchListService().getBranchList();
		request.setAttribute("branchLists", branchList);

		List<PositionList> positionList = new PositionListService().getPositionList();
		request.setAttribute("positionLists", positionList);



		if (isValid(request, messages, signUpUser) == true) {

			new UserService().register(signUpUser);

			response.sendRedirect("userlist");
			return;
		} else {
			request.setAttribute("signUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private User getSignUpUser(HttpServletRequest request)
			throws IOException, ServletException {

		User signUpUser = new User();
		signUpUser.setLoginId(request.getParameter("login_id"));
		signUpUser.setPassword(request.getParameter("password"));
		signUpUser.setName(request.getParameter("name"));
		signUpUser.setBranchId(Integer.parseInt(request.getParameter("branchLists")));
		signUpUser.setPositionId(Integer.parseInt(request.getParameter("positionLists")));

		return signUpUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, User editUser) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String userName = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branchLists"));
		int positionId =  Integer.parseInt(request.getParameter("positionLists"));
		User check = new UserService().checkId(login_id);


		if (check != null) {
			messages.add("ログインIDがすでに使用されています");
		}

		if (StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}

		if(login_id.matches("\\W*") && (!StringUtils.isBlank(login_id))){
			messages.add("ログインIDは半角英数で入力してください");
		}

		if (20 < login_id.length() || login_id.length() < 6 && (!StringUtils.isBlank(login_id))) {
			messages.add("ログインIDは6文字以上、20文字以下で入力してください");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}

		if (20 < password.length() || password.length() < 6 && (!StringUtils.isBlank(password))) {
			messages.add("パスワードは6文字以上、20文字以下で入力してください");
		}

		if(password.equals(passwordConfirm) == false){
			messages.add("確認パスワードが一致しておりません");
		}

		if (StringUtils.isBlank(userName) == true) {
			messages.add("ユーザー名を入力してください");
		}

		if (10 < userName.length()) {
			messages.add("名前は10文字以下で入力してください");
		}


		if ((branchId == 1) && (3 <= positionId)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}else if((branchId != 1) && (1 <= positionId && positionId <= 2)){
			messages.add("支店と部署・役職の組み合わせが不正です");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}