package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import beans.BranchList;
import beans.PositionList;
import beans.User;
import service.BranchListService;
import service.PositionListService;
import service.UserService;

@WebServlet(urlPatterns = { "/userlist" })
public class UserListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<User> userList = new UserService().getUsers();

		request.setAttribute("users", userList);


		List<BranchList> branchList = new BranchListService().getBranchList();
		request.setAttribute("branchLists", branchList);

		List<PositionList> positionList = new PositionListService().getPositionList();
		request.setAttribute("positionLists", positionList);

		request.getRequestDispatcher("userlist.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User activeUser = new User();

		activeUser.setId(Integer.parseInt(request.getParameter("id")));
		activeUser.setIsDeleted(Integer.parseInt(request.getParameter("active")));

		try {
			new UserService().activeChange(activeUser);
		} catch (ParseException e) {
			// フォーマット変換に失敗したらエラー
			e.printStackTrace();
			response.sendRedirect("userlist");
		}
		response.sendRedirect("userlist");
	}
}
