package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.BranchList;
import dao.BranchListDao;

public class BranchListService {

	private static final int LIMIT_NUM = 1000;
	public List<BranchList> getBranchList() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchListDao branchListDao = new BranchListDao();
			List<BranchList> ret = branchListDao.getBranchList(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
