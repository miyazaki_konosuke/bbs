package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.PositionList;
import dao.PositionListDao;

public class PositionListService {

	private static final int LIMIT_NUM = 1000;
	public List<PositionList> getPositionList() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionListDao positionListDao = new PositionListDao();
			List<PositionList> ret = positionListDao.getPositionList(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
