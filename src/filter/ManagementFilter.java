package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/setting", "/signup", "/userlist" })
public class ManagementFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// System.out.println("ログインチェック");

		// セッションが存在しない場合NULLを返す
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		HttpSession session = (HttpSession) ((HttpServletRequest) request).getSession();
//		String requestPath = ((HttpServletRequest) request).getServletPath();

		if (user.getBranchId() == 1 && user.getPositionId() == 1) {
			chain.doFilter(request, response);
		}else{
			String messages = "管理者のみアクセスできます";
			session.setAttribute("errorMessages", messages);

			((HttpServletResponse) response).sendRedirect("./");
			return;
		}
	}

	public void init(FilterConfig config) throws ServletException {

	}

	public void destroy() {

	}
}