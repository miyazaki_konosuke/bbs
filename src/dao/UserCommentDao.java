package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.message_id as message_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.insert_date as insert_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id "); //くっつける箇所のやつ
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String text = rs.getString("text");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				int user_id = rs.getInt("user_id");
				int message_id = rs.getInt("message_id");


				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setText(text);
				comment.setName(name);
				comment.setInsertDate(insertDate);
				comment.setUserId(user_id);
				comment.setMessageId(message_id);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}