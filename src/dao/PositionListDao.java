package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.PositionList;
import exception.SQLRuntimeException;

public class PositionListDao {
	public List<PositionList> getPositionList(Connection connection, int num) {

		//メッセージ一覧表示に関するdao
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("positions.id as id, ");
			sql.append("positions.name as name ");
			sql.append("FROM positions ");

			ps = connection.prepareStatement(sql.toString());

//			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<PositionList> ret = toPositionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<PositionList> toPositionList(ResultSet rs)
			throws SQLException {

		List<PositionList> ret = new ArrayList<PositionList>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");

				PositionList PositionList = new PositionList();
				PositionList.setName(name);
				PositionList.setId(id);

				ret.add(PositionList);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

}

