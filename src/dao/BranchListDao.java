package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.BranchList;
import exception.SQLRuntimeException;

public class BranchListDao {
	public List<BranchList> getBranchList(Connection connection, int num) {

		//メッセージ一覧表示に関するdao
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("branches.id as id, ");
			sql.append("branches.name as name ");
			sql.append("FROM branches ");

			ps = connection.prepareStatement(sql.toString());

//			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<BranchList> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<BranchList> toBranchList(ResultSet rs)
			throws SQLException {

		List<BranchList> ret = new ArrayList<BranchList>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");

				BranchList branchList = new BranchList();
				branchList.setName(name);
				branchList.setId(id);

				ret.add(branchList);

			}
			return ret;
		} finally {
			close(rs);
		}
	}

}

