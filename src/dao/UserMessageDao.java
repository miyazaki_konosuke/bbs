package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection,
			int num, String start, String end, String seachcategory) {

		//メッセージ一覧表示に関するdao
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("messages.title as title, ");
			sql.append("messages.category as category, ");
			sql.append("messages.text as text, ");
			sql.append("messages.insert_date as insert_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE insert_date >= ? "); //表示範囲の指定（期間開始）
			sql.append("AND insert_date <= ? "); //表示範囲の指定（期間終了）

			if(StringUtils.isBlank(seachcategory)== false){
				//saechcategory が不適格なら検索条件に加えない
				sql.append("AND category like ? ");
			}
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end + "00:00:00");

			if (StringUtils.isBlank(seachcategory)== false){
				ps.setString(3,  "%" + seachcategory + "%");
			}


			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				int user_id = rs.getInt("user_id");


				UserMessage message = new UserMessage();
				message.setName(name);
				message.setId(id);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setInsertDate(insertDate);
				message.setUserId(user_id);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}