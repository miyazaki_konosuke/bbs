<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class = "main-contents">
<c:if test="${ not empty errorMessages }">
	<div class="errorMessagesset">
		<ul>
			<c:forEach items="${errorMessages }" var="message">
				<li><c:out value="${message }" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>


		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }"><div class="headtitle">
				<a href="./">ホーム</a>
				<a href="newMessage">投稿する</a>

				<c:if test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1 }">
				<a href="userlist">ユーザー管理</a>
				</c:if></div>
				<div class="logout">
				<a href="logout" class="logout">ログアウト</a>
				</div>
			</c:if>
		</div>

<form action="signup" method="post"><br />
	<div class="settings">
	<label for="login_id">ログインID(6文字以上、20文字以下)</label><div class="waku">
	<input name="login_id" id="login_id" value="${signUpUser.getLoginId()}" /><br /></div><br />

	<label for="password">パスワード(6文字以上、20文字以下)</label><div class="waku">
	<input name="password" type="password" id="password"/><br /><br />

	  <label for="passwordConfirm">確認パスワード</label><div class="waku">
  <input type="password" name="passwordConfirm" id="passwordConfirm" ><br /></div><br />

	<label for="name">ユーザー名(10文字以下)</label><div class="wakus">
	<input name="name" id="name" value="${signUpUser.name}" /></div></div>


	<!-- セレクトボックス（支店） -->

	<p>支店
		<select name="branchLists">
			<c:forEach items="${branchLists}" var="branchList">

			<!-- セレクトボックスの値の保持（参照可能） -->
				支店<c:if test="${signUpUser.getBranchId() == branchList.id }">
				<option  value="${branchList.id}"selected = "${signUpUser.getBranchId()}">
				<c:out value="${branchList.name}" /></option></c:if>


			<!-- セレクトボックスの値の保持（参照不可） -->
				<c:if test="${signUpUser.getBranchId() != branchList.id }">
				<option  value="${branchList.id}">
				<c:out value="${branchList.name}" /></option></c:if>

			</c:forEach>
		</select>
	</p>


	<!-- セレクトボックス（部署） -->

		<p>部署・役職
		<select name="positionLists">
			<c:forEach items="${positionLists}" var="positionList">

			<!-- セレクトボックスの値の保持（参照可能） -->
				<c:if test="${signUpUser.getPositionId() == positionList.id }">
				<option  value="${positionList.id}"selected = "${signUpUser.getPositionId()}">
				<c:out value="${positionList.name}" /></option></c:if>
			<!-- セレクトボックスの値の保持（参照不可） -->
				<c:if test="${signUpUser.getPositionId() != positionList.id }">
				<option  value="${positionList.id}">
				<c:out value="${positionList.name}" /></option></c:if>

			</c:forEach>
		</select>
	</p>
	<input type="submit" value="登録" /> <br />
	</div>
</form>
</div>



</body>
<div class="copyright">(c)Konosuke Miyazaki</div>
</html>