<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー編集</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }"><div class="headtitle">
				<a href="./">ホーム</a>
				<a href="newMessage">投稿する</a>

				<c:if test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1 }">
				<a href="userlist">ユーザー管理</a>
				</c:if></div>
				<div class="logout">
				<a href="logout" class="logout">ログアウト</a>
				</div>
			</c:if>
		</div>




<c:if test="${ not empty errorMessages }">
	<div class="errorMessagesset">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<form action="settings" method="post"><br />
<div class="settings">

	<input type="hidden" name="id" value= "${editUser.id}">

	<label for="login_id">ログインID(6文字以上、20文字以下)</label><div class="waku">
	<input name="login_id" id = "login_id" value= "${editUser.getLoginId()}"/></div><br />



	<label for="password">パスワード(6文字以上、20文字以下)</label><div class="waku">
	<input name="password" type="password" id="password"/></div> <br />

	<label for="passwordConfirm">確認パスワード</label><br />
	<input name="passwordConfirm" type="password" id="passwordConfirm"/><br /><br />

	<label for="name">ユーザー名(10文字以下)</label><div class="waku">
	<input name="name"  id="name" value="${editUser.name }"/></div>
	<!-- 自分のアカウント編集の場合、セレクトボックス非表示 -->
	<c:if test="${loginUser.getId() == editUser.id }" >

	<p>
			<c:forEach items="${branchLists}" var="branchList" >
			<input type = "hidden" name="branchLists" value="${branchList.id}" >
			</c:forEach>
	</p>


	<p>
			<c:forEach items="${positionLists}" var="positionList">
			<input type = "hidden" name="positionLists" value="${positionList.id}" >
			</c:forEach>
	</p>


<!-- セレクトボックス（支店） -->

</c:if>

	<c:if test="${loginUser.getId() != editUser.id }" >

	<p>支店
		<select name="branchLists">
			<c:forEach items="${branchLists}" var="branchList">

			<!-- セレクトボックスの値の保持（参照可能） -->
				<c:if test="${editUser.getBranchId() == branchList.id }">
				<option  value="${branchList.id}"selected = "${editUser.getBranchId()}">
				<c:out value="${branchList.name}" /></option></c:if>


			<!-- セレクトボックスの値の保持（参照不可） -->
				<c:if test="${editUser.getBranchId() != branchList.id }">
				<option  value="${branchList.id}">
				<c:out value="${branchList.name}" /></option></c:if>

			</c:forEach>
		</select>
	</p>


	<!-- セレクトボックス（部署） -->

		<p>部署・役職
		<select name="positionLists">
			<c:forEach items="${positionLists}" var="positionList">

			<!-- セレクトボックスの値の保持（参照可能） -->
				<c:if test="${editUser.getPositionId() == positionList.id }">
				<option  value="${positionList.id}"selected = "${editUser.getPositionId()}">
				<c:out value="${positionList.name}" /></option></c:if>
			<!-- セレクトボックスの値の保持（参照不可） -->
				<c:if test="${editUser.getPositionId() != positionList.id }">
				<option  value="${positionList.id}">
				<c:out value="${positionList.name}" /></option></c:if>

			</c:forEach>
		</select>
	</p>

</c:if>

	<input type="submit" value="登録" /> <br />
	</div>
</form>
<div class="copyright">(c)Konosuke Miyazaki</div>
</div>
</body>
</html>
