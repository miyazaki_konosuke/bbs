<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessageslist">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

				<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">ユーザー登録</a>
			</c:if>
			<c:if test="${ not empty loginUser }"><div class="headtitle">
				<a href="./">ホーム</a>
				<a href="newMessage">投稿する</a>

				<c:if test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1 }">
				<a href="signup">ユーザー登録</a>
				</c:if>
				</div>
				<div class="logout">
				<a href="logout" class="logout">ログアウト</a>
				</div>
			</c:if>
		</div>
<body>

	<div class="main-contents">
<table class="tablemain">
  <tr style =
  "
    border: 1px solid #DDD;
    ">


    <th>支店名</th>
    <th>部署・役職</th>
    <th>ログインID</th>
    <th>ユーザー名(編集)</th>
    <th>稼動状況</th>
    <th>復活・停止</th>
  </tr>
<c:forEach var="userList" items="${users}">
  <tr>
    <!-- DB連携による支店名の表示 -->


    <td>
    	<c:forEach items="${branchLists}" var="branchList">
    		<c:if test="${userList.getBranchId() == branchList.id}">
    ${branchList.name}
    		</c:if>
    	</c:forEach>
    </td>



    <td>
    	<c:forEach items="${positionLists}" var="positionList">
    		<c:if test="${userList.getPositionId() == positionList.id}">
    ${positionList.name}
    		</c:if>
    	</c:forEach>
    </td>
    <td>${userList.getLoginId()}</td>
    <td><a href="settings?id=${userList.id}">${userList.getName()}</a></td>

    <td class="teisi">

    <c:if test="${loginUser.id != userList.id}">
    	<form action="userlist" method="post">
    		<input type="hidden" name="id" value= "${userList.id}">
    		<c:if test="${userList.getIsDeleted() == 0}">
	<div class="active">
    			<c:out  value="アクティブ"/></div>
    		</c:if>
    		<c:if test="${userList.getIsDeleted() == 1}">
    			<div class="stop"><c:out  value="停止中"/></div>
    		</c:if>
    	</form>
    	</c:if>

    	</td>

    <!-- 復活停止切り替えボタン -->
    <td>
    	<c:if test="${loginUser.id != userList.id}">
    	<form action="userlist" method="post">
    		<input type="hidden" name="id" value= "${userList.id}">
    		<c:if test="${userList.getIsDeleted() == 0}">
    			<button type="submit" name="active" value="1"
onclick="return confirm('停止しますか？')">停止する</button>
    		</c:if>
    		<c:if test="${userList.getIsDeleted() == 1}">
    			<button type="submit" name="active" value="0"
onclick="return confirm('復活しますか？')">復活する</button>
    		</c:if>
    	</form>
    	</c:if>
    </td>

    <td></td>
  </tr>
</c:forEach>

</table>
</div>
	<div class="copyright">(c)Konosuke Miyazaki</div>
</body>
</html>