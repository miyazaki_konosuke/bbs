

<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板課題</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">




<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<div class="headtitle">
				<a href="./">ホーム</a>
				<a href="newMessage">投稿する</a>

				<c:if test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1 }">
				<a href="userlist">ユーザー管理</a>
				</c:if></div>
				<div class="logout">
				<a href="logout" class="logout">ログアウト</a>
				</div>
			</c:if>
		</div>

<div class="seach">
		<!-- 日付指定のセレクトボックス -->
	<form action="./" method="get">投稿表示期間指定
		<input type="date" name="start" max="9999-12-31" value="${start}" > ～
		<input type="date" name="end" max="9999-12-31" value="${end}" ><br />


	<!-- カテゴリー検索のテキストボックス（部分一致） --><div class="blank">
			<label for="seachcategory">カテゴリー部分一致指定</label>
			<input name="seachcategory" id="seachcatergory" value="${seachcategory}" />
		<input type=submit value="検索" /></div>
		<div class="link"><a href="./">検索リセット</a></div>
	</form>
</div>



	  <br />
		<div class="messages">
			<c:forEach items="${messages}" var="message">
			<input type="hidden" name="id" value="${message.id}">
				<div class="message">





					<div class="messagetitle">

						【タイトル】<c:out value="${message.title}" />

					</div>


					<div class="messagehon">
						【本文】
					</div>
					<div class="messagetext">
						 <c:out value="${message.text}" />
					</div>

					<div class="messagetop">

					</div>
		 			<div class="messagecategory">
					<span class="category">【カテゴリー】<c:out
							value="${message.category}" /></span><br />
					</div>

					<div class="messagetop">

					</div>
							 <span class="id"><input
						type=hidden value="${message.id}"></span>
					<div class="messagedate">【登録日】
						<fmt:formatDate value="${message.insertDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<div class="messagetop">

					</div>
					<div class="messagename">【投稿ユーザー】
						 <span class="name"><c:out
								value="${message.name}" /></span>
					</div>

			<c:if test= "${loginUser.id == message.getUserId()}" >
					<form action="messageDelete" method="post">
						<input type="hidden" name="id" value= "${message.id}">

						<div class="messagedelete">
							<button type="submit" name="delete" value="0" onclick="return confirm('削除しますか？')">投稿削除</button> <br />
						</div>

					</form>
			</c:if>
				</div>


<!-- コメント表示部分 -->
		<c:forEach items="${comments}" var="comment">
<c:if test= "${comment.getMessageId() == message.id}" >
				<div class="comments">

					<br /> 【この投稿へのコメント】<br /><div class="commenttext"><c:out value="${comment.text}"  /></div>
					<br /> 【コメントユーザー】<br /> <div class="commenttext"><div class="commentuser"><c:out value="${comment.name}" />
<br /> 【投稿日】<br /><c:out value="${comment.insertDate}" />
					<c:if test= "${loginUser.id == comment.getUserId()}" >
				<form action="commentDelete" method="post">
						<input type="hidden" name="comment_id" value= "${comment.id}">
				<div class="commentsdelete">
						<button type="submit" name="delete" value="0" onclick="return confirm('削除しますか？')">コメント削除</button>
				</div>
				</form>
</c:if></div></div><br />


				</div>
		</c:if>
		</c:forEach>

<div class="newcomment">
					<form action="newComment" method="post">

						<input type="hidden" name="message_id" value="${message.id}">
						コメントする（500文字以下）<br />
						<textarea name="comment" cols="40" rows="6" class="tweet-box"  ></textarea>
						<br /> <input type="submit" value="コメントする"><span
							class="id"></span>
					</form>
							</div>
			</c:forEach>
				</div>
				<br />
				<br />

		</div>

		<div class="copyright">(c)Konosuke Miyazaki</div>

</body>
</html>
