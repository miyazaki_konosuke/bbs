<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
		<c:if test="${ not empty errorMessages }">
	<div class="errorMessagesmes">
		<ul>
			<c:forEach items="${errorMessages}" var="comment">
				<li><c:out value="${comment}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>


		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }"><div class="headtitle">
				<a href="./">ホーム</a>
				<a href="newMessage">投稿する</a>

				<c:if test="${loginUser.getBranchId() == 1 && loginUser.getPositionId() == 1 }">
				<a href="userlist">ユーザー管理</a>
				</c:if></div>
				<div class="logout">
				<a href="logout" class="logout">ログアウト</a>
				</div>
			</c:if>
		</div>
<body>

<br /><br />
	<div class="main-contents">

		<div class="form-area">
				<form action="newMessage" method="post">
					タイトル（30文字以下）<br />

					<input type="text" name="title" size="30" maxlength="40" value="${message.title}"><br />
					<br />

					カテゴリー（10文字以下）<br />

					<input type="text" name="category" size="30" maxlength="20" value="${message.category}"><br />
					<br />

					本文（1000文字以下）<br />

					<textarea name="message" cols="100" rows="10" class="tweet-box" >${message.text}</textarea><br />
					<br /> <input type="submit" value="投稿する">
				</form>
		</div>

		<div class="copyright">(c)Konosuke Miyazaki</div>
	</div>
</body>
</html>
